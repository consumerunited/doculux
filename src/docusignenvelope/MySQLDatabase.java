package docusignenvelope;
        import java.sql.*;
        import java.util.logging.*;

public class MySQLDatabase{
    private Connection con =  null;
    private final String url;
    private final String user;
    private final String password;

    public MySQLDatabase(String url, String user, String password) {
        this.url = url;
        this.user = user;
        this.password = password;
    }
    public Connection getConnection(){

        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection(url, user, password);

        } catch (ClassNotFoundException | SQLException ex) {
        }
        return con;
    }
    public void close(){
        try {
            if (con != null) {
                con.close();
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
