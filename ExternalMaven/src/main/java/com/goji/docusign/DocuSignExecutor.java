/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.goji.docusign;

import com.goji.docusign.json.model.AuthenticationHeader;
import com.goji.docusign.json.model.Folder;
import com.goji.docusign.json.model.FolderItem;
import com.goji.docusign.json.model.FolderItemList;
import com.goji.docusign.json.model.Receipients;
import com.goji.docusign.utl.DefaultAccess;
import com.goji.docusign.utl.EnvelopeAccess;
import com.goji.docusign.utl.SalesforceAccess;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author paul.ye
 */
public class DocuSignExecutor extends DefaultAccess {

    private static EnvelopeAccess ea;
    private static String demoId;
    private static AuthenticationHeader auth;
    private static SalesforceAccess sa;

    private void init() throws IOException {
        auth = new AuthenticationHeader();
        auth.setIntegratorKey(DEV_API_KEY);
        auth.setPassword("minimed1!2@3#4$5%");
        auth.setUsername("mackenzie.allen@goji.com");
        demoId = PROD_ACCOUNT_ID;

        ea = new EnvelopeAccess();
        ea.setIsTest(false);
        ea.login(auth);
        sa = new SalesforceAccess();
        sa.setIsTest(false);
    }
    
    public DocuSignExecutor() throws IOException{
        init();
    }

    public static void main(String[] args) throws IOException {
        DocuSignExecutor exe = new DocuSignExecutor();
        exe.run();
    }

    private void run() {
        Timer t1 = new Timer();
        t1.scheduleAtFixedRate(new TimerTask() {
               @Override
                public void run(){
                    new DailyRun().run();
                }
        },0,1000*60*15); //every 15 mins

        Timer t2 = new Timer();
        t2.scheduleAtFixedRate(new TimerTask() {
               @Override
                public void run(){
                    new WeeklyRun().run();
                }
        },0,1000*60*60*2); //every 2 hour
        
        Timer t3 = new Timer();
        t3.scheduleAtFixedRate(new TimerTask() {
               @Override
                public void run(){
                    new MonthlyRun().run();
                }
        },0,1000*60*60*24); //every day
    }

    public class DailyRun implements Runnable {

        public void run() {
            System.out.println("[D]" + Calendar.getInstance().getTime());
            try {
                if (ea == null || demoId == null || auth == null || sa == null) {
                    if (ea == null) {
                        System.out.println("param is null ");
                    }
                }
                Folder sent = ea.getSentFolderByEmail("service@goji.com", auth, demoId);
                List<String> envelopIds = new ArrayList<String>();
                FolderItemList sentList = ea.getCurDayEnvelopes(sent, auth, demoId);
                for (FolderItem item : sentList.getFolderItems()) {
                    envelopIds.add(item.getEnvelopeId());
                }
                Map<String, Receipients> result = ea.getEnvelopeStatus(envelopIds, demoId, auth);
                LOG.info("result size: " + (result == null ? 0 : result.size()));

                for (String key : result.keySet()) {
                    Receipients recp = result.get(key);
                    sa.updateDocuStatusByLeadEmail(recp.getSigners().get(0).getEmail(), recp.getSigners().get(0).getStatus(), recp.getEnvelopId());
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    public class WeeklyRun implements Runnable {

        public void run() {
            System.out.println("[W]" + Calendar.getInstance().getTime());
            try {
                Folder sent = ea.getSentFolderByEmail("service@goji.com", auth, demoId);
                List<String> envelopIds = new ArrayList<String>();
                FolderItemList sentList = ea.getCurWeekEnvelopes(sent, auth, demoId);
                for (FolderItem item : sentList.getFolderItems()) {
                    envelopIds.add(item.getEnvelopeId());
                }
                Map<String, Receipients> result = ea.getEnvelopeStatus(envelopIds, demoId, auth);
                LOG.info("result size: " + (result == null ? 0 : result.size()));

                for (String key : result.keySet()) {
                    Receipients recp = result.get(key);
                    sa.updateDocuStatusByLeadEmail(recp.getSigners().get(0).getEmail(), recp.getSigners().get(0).getStatus(), recp.getEnvelopId());
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    public class MonthlyRun implements Runnable {

        public void run() {
            System.out.println("[M]" + Calendar.getInstance().getTime());
            try {
                Folder sent = ea.getSentFolderByEmail("service@goji.com", auth, demoId);
                List<String> envelopIds = new ArrayList<String>();
                FolderItemList sentList = ea.getCurMonthEnvelopes(sent, auth, demoId);
                for (FolderItem item : sentList.getFolderItems()) {
                    envelopIds.add(item.getEnvelopeId());
                }
                Map<String, Receipients> result = ea.getEnvelopeStatus(envelopIds, demoId, auth);
                LOG.info("result size: " + (result == null ? 0 : result.size()));

                for (String key : result.keySet()) {
                    Receipients recp = result.get(key);
                    sa.updateDocuStatusByLeadEmail(recp.getSigners().get(0).getEmail(), recp.getSigners().get(0).getStatus(), recp.getEnvelopId());
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
}
