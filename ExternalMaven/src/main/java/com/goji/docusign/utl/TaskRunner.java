/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.goji.docusign.utl;

import static com.goji.docusign.utl.DefaultAccess.DEV_API_KEY;
import com.goji.docusign.json.model.AuthenticationHeader;
import com.goji.docusign.json.model.Folder;
import com.goji.docusign.json.model.FolderItem;
import com.goji.docusign.json.model.FolderItemList;
import com.goji.docusign.json.model.Receipients;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

/**
 *
 * @author paul.ye
 */
public class TaskRunner extends DefaultAccess {

    public static void main(String[] args) throws IOException, NoSuchAlgorithmException, KeyManagementException, KeyStoreException, UnrecoverableKeyException {

//        new TaskRunner().runUserSample();
        new TaskRunner().runEnvelopSample();
    }

    private void runEnvelopSample() throws IOException {
        AuthenticationHeader auth = new AuthenticationHeader();
        auth.setIntegratorKey(DEV_API_KEY);
        auth.setPassword("minimed1!2@3#4$5%");
        auth.setUsername("mackenzie.allen@goji.com");
//        String demoId = DEV_ACCOUNT_ID;
//        auth.setPassword("1227Boston!");
//        auth.setUsername("service@goji.com");
        String demoId = PROD_ACCOUNT_ID;

        EnvelopeAccess ea = new EnvelopeAccess();
        ea.setIsTest(false);
        ea.login(auth);

//        Folder draft = ea.getDraftFolderByEmail("service@goji.com", auth, demoId);
        Folder sent = ea.getSentFolderByEmail("service@goji.com", auth, demoId);
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, -7);

        List<String> envelopIds = new ArrayList<String>();
//        FolderItemList draftList = ea.getEnvelopes(draft, auth, demoId, c);
        FolderItemList sentList = ea.getEnvelopes(sent, auth, demoId, c);
//        for (FolderItem item : draftList.getFolderItems()) {
//            envelopIds.add(item.getEnvelopeId());
//        }
        for (FolderItem item : sentList.getFolderItems()) {
            envelopIds.add(item.getEnvelopeId());
        }
        Map<String, Receipients> result = ea.getEnvelopeStatus(envelopIds, demoId, auth);
        LOG.info("result size: " + (result == null ? 0 : result.size()));

        SalesforceAccess sa = new SalesforceAccess();
        for (String key : result.keySet()) {
            Receipients recp = result.get(key);
            sa.updateDocuStatusByLeadEmail(recp.getSigners().get(0).getEmail(), recp.getSigners().get(0).getStatus(), recp.getEnvelopId());
        }
    }

    private void runUserSample() throws IOException {
        AuthenticationHeader auth = new AuthenticationHeader();
        auth.setIntegratorKey(DEV_API_KEY);
        auth.setPassword("minimed1!2@3#4$5%");
        auth.setUsername("mackenzie.allen@goji.com");
//        String demoId = DEV_ACCOUNT_ID;
//        auth.setPassword("1227Boston!");
//        auth.setUsername("service@goji.com");
        String demoId = PROD_ACCOUNT_ID;

        UserAccess ua = new UserAccess();
        ua.setIsTest(false);
        ua.login(auth);
        ua.getAllUsers(auth, demoId);

//        ua.setShareToAdmin(auth, demoId, "87430e05-20a0-4504-8080-44cc0153d7c6");
//        ua.getEveryOne(auth, demoId);
//        Users us = ua.getAllUsers(auth, demoId);
//        ua.getUserDetailByEmail("paul.ye@goji.com", auth, demoId);
//        ua.DoNotCallAgain(ua, auth, demoId, "paul.ye@goji.com");
//        ua.createUser(auth, demoId, "paul.ye.docusign@goji.com");
    }

    private void sslTest() throws IOException, NoSuchAlgorithmException, KeyManagementException, KeyStoreException, UnrecoverableKeyException {
//        TrustStrategy acceptingTrustStrategy = new TrustStrategy() {
//
//            public boolean isTrusted(X509Certificate[] xcs, String string) throws CertificateException {
//                return true;
//            }
//
//        };
//        SSLSocketFactory sf = new SSLSocketFactory(acceptingTrustStrategy,
//                SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
//        SchemeRegistry registry = new SchemeRegistry();
//        registry.register(new Scheme("https", 443, sf));
//        ClientConnectionManager ccm = new PoolingClientConnectionManager(registry);
//        DefaultHttpClient httpClient = new DefaultHttpClient(ccm);  
        /**
         * peer shutdown during handshake, 1) could be multiple protocol
         */
//        DefaultHttpClient httpClient = new DefaultHttpClient();
//        String urlOverHttps = "https://10.100.26.151:443/void";
//        HttpGet getMethod = new HttpGet(urlOverHttps);
//        HttpResponse response = httpClient.execute(getMethod);
//        System.out.println(response.getStatusLine().getStatusCode());
        

        HttpURLConnection connection = (HttpURLConnection) new URL("https://10.100.26.151:443/void/").openConnection();
        connection.setRequestMethod("GET");
        connection.setConnectTimeout(10000);
        int responseCode = connection.getResponseCode();
        System.out.println(responseCode);
    }
}
