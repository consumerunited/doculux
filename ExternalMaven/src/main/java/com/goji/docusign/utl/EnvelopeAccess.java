/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.goji.docusign.utl;

import com.goji.docusign.json.model.AuthenticationHeader;
import com.goji.docusign.json.model.FolderItemList;
import com.goji.docusign.json.model.Folder;
import com.goji.docusign.json.model.FolderItem;
import com.goji.docusign.json.model.Folders;
import com.goji.docusign.json.model.Receipients;
import com.goji.docusign.json.model.User;
import com.goji.docusign.json.model.UserInfo;
import com.goji.docusign.json.model.Users;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;

/**
 *
 * @author paul.ye
 */
public class EnvelopeAccess extends DefaultAccess{
       
    private Folders getFolderList(AuthenticationHeader auth, String demoId) throws JsonProcessingException, IOException {
        String url = "/accounts/" + demoId  + "/folders";
        String resp = restCall(auth, url, "GET", "");
        ObjectMapper mapper = new ObjectMapper();
        Folders f = mapper.readValue(resp, Folders.class);
        if (f != null) {
            LOG.info("Folder List: \n" + f);
        }
        return f;
    }


    Folder getDraftFolderByEmail(String email, AuthenticationHeader auth, String demoId) throws IOException{
        return getFolderByEmail(email, auth, demoId, "draft");
    }
    
    public Folder getSentFolderByEmail(String email, AuthenticationHeader auth, String demoId) throws IOException{
        return getFolderByEmail(email, auth, demoId, "sentitems");
    }
    
    private Folder getFolderByEmail(String email, AuthenticationHeader auth, String demoId, String folderType) throws IOException{
        if(email==null || email.length()<1 || (!email.matches("\\w+\\.\\w+@goji.com")&&!email.matches("\\w+@goji.com"))){
            LOG.severe("Invalid Email to find a folder");
            return null;
        }
        if(folderType==null || folderType.length()<1){
            LOG.severe("invalid folder type!");
            return null;
        }
        
        UserAccess ua = new UserAccess();
        ua.setIsTest(isTest);
        LOG.info("trying to get all users");
        Users us = ua.getAllUsers(auth, demoId);
        Set<String> activeIds = new HashSet<String>();
        for(User u : us.getUsers()){
            if(email.equalsIgnoreCase(u.getEmail())){
                if("Active".equalsIgnoreCase(u.getUserStatus())){
                    if(!activeIds.contains(u.getUserId())){
                        LOG.info("active user found by email: " + email + ", name: " + u.getUserName() + ", [" + u.getUserId() + "]");
                        activeIds.add(u.getUserId());
                    }
                }
            }
        }
        Folders fs = getFolderList(auth, demoId);
        for(Folder f: fs.getFolders()){
            if(folderType.equalsIgnoreCase(f.getType()) && activeIds.contains(f.getOwnerUserId())){
                return f;
            }
        }
        return null;
    }
    
    FolderItemList getEnvelopes(Folder f, AuthenticationHeader auth, String demoId, Calendar c) throws JsonProcessingException, IOException{
        String url = "/accounts/" + demoId  + "/folders/" + f.getFolderId() + "/";
        String qParam1 = "?from_date=" + (c.get(Calendar.MONTH)+1) + "%2F" + c.get(Calendar.DATE) + "%2F" + c.get(Calendar.YEAR);  // url encoded
        String qParam2 = "";  //        String qParam2 = "&status=created"; search across folder, not only created
        url += qParam1 + qParam2;
        String resp = restCall(auth, url, "GET", "");
        FolderItemList fl = new ObjectMapper().readValue(resp, FolderItemList.class);
        LOG.info("Env list: " + fl);
        return fl;
    }
    
    FolderItemList getEnvelopesByPeriod(Folder f, AuthenticationHeader auth, String demoId, Calendar c1, int dateInterval) throws JsonProcessingException, IOException{
        String url = "/accounts/" + demoId  + "/folders/" + f.getFolderId() + "/";
        Calendar c2 = Calendar.getInstance();
        c2.add(Calendar.DATE, dateInterval*(-1));
        LOG.info("ranging : " + c2.getTime() + " - " + c1.getTime());
        String qParam1 = "?from_date=" + (c2.get(Calendar.MONTH)+1) + "%2F" + c2.get(Calendar.DATE) + "%2F" + c2.get(Calendar.YEAR);  // url encoded
        String qParam2 = "&to_date=" + (c1.get(Calendar.MONTH)+1) + "%2F" + c1.get(Calendar.DATE) + "%2F" + c1.get(Calendar.YEAR) ;  //        String qParam2 = "&status=created"; search across folder, not only created
        url += qParam1 + qParam2;
        String resp = restCall(auth, url, "GET", "");
        FolderItemList fl = new ObjectMapper().readValue(resp, FolderItemList.class);
        LOG.info("Env list: " + fl);
        return fl;
    }
    
    public FolderItemList getCurDayEnvelopes(Folder f, AuthenticationHeader auth, String demoId) throws JsonProcessingException, IOException{
        Calendar c = Calendar.getInstance();
        return getEnvelopes(f, auth, demoId, c);
    }
    
    public FolderItemList getCurWeekEnvelopes(Folder f, AuthenticationHeader auth, String demoId) throws JsonProcessingException, IOException{
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, -1);
        return getEnvelopesByPeriod(f, auth, demoId, c, 5); //30 cur, then 29-5=24~29 + 30 is cur week, total 7 days
    }
    
    public FolderItemList getCurMonthEnvelopes(Folder f, AuthenticationHeader auth, String demoId) throws JsonProcessingException, IOException{
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, -7);
        return getEnvelopesByPeriod(f, auth, demoId, c, 22); //30 cur, 24 cur week, then 30-7=23.  23-22 = 1 ~ 23 is cur month, total 23 + 7 = 30 days
    }
    
    public Map<String, Receipients> getEnvelopeStatus(List<String> envelopes, String demoId, AuthenticationHeader auth) throws JsonProcessingException, IOException {
       Map<String, Receipients> envelopeToStatus = new HashMap();
       for(String envelope : envelopes) {
           JsonPath.parse(envelopes);
           String url = "/accounts/" + demoId + "/envelopes/" + envelope + "/recipients";
           String response = restCall(auth, url, "GET", "");
           Receipients tempRcp = new ObjectMapper().readValue(response, Receipients.class);
           if(tempRcp.getSigners()==null || tempRcp.getSigners().size()<1){
               LOG.warning("GET Signer failed!! - env Id: " + envelope + " \n" + response);
               continue;
           }
           tempRcp.setEnvelopId(envelope);
           envelopeToStatus.put(envelope, tempRcp);
       }
       return envelopeToStatus;
   }


    
}
