/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.goji.docusign.utl;

import com.goji.docusign.json.model.AuthenticationHeader;
import com.goji.docusign.json.model.ForgottenPasswordInfo;
import com.goji.docusign.json.model.Group;
import com.goji.docusign.json.model.Groups;
import com.goji.docusign.json.model.User;
import com.goji.docusign.json.model.NewUsers;
import com.goji.docusign.json.model.SharedAccess;
import com.goji.docusign.json.model.SharedAccessItem;
import com.goji.docusign.json.model.SharedEnvelope;
import static com.goji.docusign.json.model.User.newAgent;
import com.goji.docusign.json.model.Users;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * example id: 8b1aeea1-dacf-44ca-8c2c-f74ee46adf50 closed reopen: same Id 1)
 * Creation only sends the email for the first time. Every time after that, has
 * to do it manually.
 *
 * @author paul.ye
 */
public class UserAccess extends DefaultAccess {

    private void createNewUser(AuthenticationHeader auth, String demoId, User u) throws JsonProcessingException, IOException {
        NewUsers nu = new NewUsers();
        nu.setNewUsers(new ArrayList<User>());
        nu.getNewUsers().add(u);
        //p277
        String url = "/accounts/" + demoId + "/users";
        String resp = restCall(auth, url, "POST", new ObjectMapper().writeValueAsString(nu));
        LOG.info("Creating user: \n" + resp);
    }

    private void closeUser(User u, AuthenticationHeader auth, String demoId) throws JsonProcessingException {
        Users us = new Users();
        us.setUsers(new ArrayList<User>());
        us.getUsers().add(u);
        String url = "/accounts/" + demoId + "/users";
        String resp = restCall(auth, url, "DELETE", new ObjectMapper().writeValueAsString(us));
        LOG.info("Closing user: \n" + resp);
    }

    /**
     * Method requires additional model for all the detail fields. Not needed
     * yet.
     *
     * @param userId
     * @param auth
     * @param demoId
     * @return
     * @throws JsonProcessingException
     */
    private User getUserDetail(String userId, AuthenticationHeader auth, String demoId) throws JsonProcessingException {
        String url = "/accounts/" + demoId + "/users/" + userId;
        String resp = restCall(auth, url, "GET", "");
        LOG.info("User detail: \n" + resp);
        url += "/profile";
        resp = restCall(auth, url, "GET", "");
        LOG.info("User profile: \n" + resp);
        return null;
    }

    private Groups getGroupInfo(AuthenticationHeader auth, String demoId) throws JsonProcessingException, IOException {
        //p218
        String url = "/accounts/" + demoId + "/groups";
        String resp = restCall(auth, url, "GET", "");

        Groups groups = new ObjectMapper().readValue(resp, Groups.class);
        LOG.info("Group Information: \n" + groups);
        return groups;
    }

    private Users getUsersInGroup(AuthenticationHeader auth, String demoId, String groupType) throws IOException {
        Group groupList = null;
        Groups groups = getGroupInfo(auth, demoId);
        if (groups == null) {
            LOG.severe("There is no group");
            return null;
        }
        for (Group g : groups.getGroups()) {
            if (groupType.equalsIgnoreCase(g.getGroupType())) {
                groupList = g;
            }
        }
        if (groupList == null) {
            LOG.severe("Cannot find the group for everyone");
            return null;
        }
        String url = "/accounts/" + demoId + "/groups/" + groupList.getGroupId() + "/users";
        //GET https://{server}/restapi/{apiVersion}/accounts/{accountId}/groups/{groupId}/users
        String resp = restCall(auth, url, "GET", "");
        Users us = new ObjectMapper().readValue(resp, Users.class);
        LOG.info("Users in " + groupType + ": \n" + us);
        return us;
    }

    private Users getEveryOne(AuthenticationHeader auth, String demoId) throws IOException {
        return getUsersInGroup(auth, demoId, "everyoneGroup");
    }

    Users getAllUsers(AuthenticationHeader auth, String demoId) throws JsonProcessingException, IOException {
        String url = "/accounts/" + demoId + "/users";
        String resp = restCall(auth, url, "GET", "");
        Users us = new ObjectMapper().readValue(resp, Users.class);
        LOG.info("Users List : \n" + us);
        return us;
    }

    private User getUserDetailByEmail(String email, AuthenticationHeader auth, String demoId) throws IOException {
        if (email == null || email.length() < 1) {
            return null;
        }
        Users us = getAllUsers(auth, demoId);
        String id = null;
        for (User u : us.getUsers()) {
            if (email.equalsIgnoreCase(u.getEmail())) {
                id = u.getUserId();
            }
        }
        if (id == null) {
            return null;
        }
        return getUserDetail(id, auth, demoId);
    }
    
    //    private String getUserSettings(AuthenticationHeader auth, String url){
//        String 
//    }
    private void getSharedAccess(AuthenticationHeader auth, String demoId) throws JsonProcessingException {
        String url = "/accounts/" + demoId + "/shared_access";
        String resp = restCall(auth, url, "GET", "");
        LOG.info("Shared access: \n" + resp);
    }

    private void setAShareToB(AuthenticationHeader auth, String demoId, String userA_Id, String userB_Id) throws JsonProcessingException {
        getSharedAccess(auth, demoId);
        SharedAccess sa = new SharedAccess();
        sa.setSharedAccess(new ArrayList<SharedAccessItem>());

        SharedAccessItem sai = new SharedAccessItem();
        User userB = new User();
        userB.setUserId(userB_Id);
        sai.setUser(userB);

        SharedEnvelope se = new SharedEnvelope();
        User userA = new User();
        userA.setUserId(userA_Id);
        se.setUser(userA);
        se.setShared("shared_to");
        sai.setEnvelopes(new ArrayList<SharedEnvelope>());
        sai.getEnvelopes().add(se);
        sa.getSharedAccess().add(sai);
        String url = "/accounts/" + demoId + "/shared_access";
        String resp = restCall(auth, url, "PUT", new ObjectMapper().writeValueAsString(sa));
    }

    private void setShareToAdmin(AuthenticationHeader auth, String demoId, String userId) throws JsonProcessingException {
        setAShareToB(auth, demoId, userId, "9844b818-5e92-48c4-97a5-21cdb76a36df");
    }

    ///////////////////////////////////// CAUTION !!!!!!!!!!!!! ///////////////////////////////////////
    private void createUser(AuthenticationHeader auth, String demoId, String email) throws IOException {
        String password = "1227Boston!";
        String firstName = "Paul";
        String lastName = "Test";
        String middleName = "Pilot";
        ForgottenPasswordInfo defaultInfo = new ForgottenPasswordInfo();
        defaultInfo.setForgottenPasswordQuestion1("What is a group of 8 Hobbits?");
        defaultInfo.setForgottenPasswordAnswer1("Hobyte");
        List<Group> groupList = new ArrayList<Group>();
        Groups groups = getGroupInfo(auth, demoId);
        if (groups == null) {
            LOG.severe("There is no group");
            return;
        }
        for (Group g : groups.getGroups()) {
            if ("everyoneGroup".equalsIgnoreCase(g.getGroupType())) {
                if (g.getPermissionProfileId() == null) {

                }
                groupList.add(g);
                LOG.finer("groupList Found : \n" + new ObjectMapper().writeValueAsString(g));
            }
        }
        User u = newAgent(email, password, firstName, lastName, middleName, groupList, defaultInfo);
        createNewUser(auth, demoId, u);
    }

    private void DoNotCallAgain(UserAccess ua, AuthenticationHeader auth, String demoId, String email) throws IOException {
        Users us = ua.getAllUsers(auth, demoId);
        List<User> toClose = new ArrayList<User>();
        for (User u : us.getUsers()) {
            if (email.equalsIgnoreCase(u.getEmail())) {
                toClose.add(u);
            }
        }
        if (toClose.size() > 0) {
            for (User u : toClose) {
                User u_copy = new User();
                u_copy.setUserId(u.getUserId());
                u_copy.setUserName(u.getUserName());
                ua.closeUser(u_copy, auth, demoId);
            }
        }
        ua.getAllUsers(auth, demoId);
    }

    private void DoNotCall(UserAccess ua, AuthenticationHeader auth, String demoId) throws IOException {
        DoNotCallAgain(ua, auth, demoId, "paulyy0701@gmail.com");
    }
}
