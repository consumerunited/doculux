/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.goji.docusign.utl;

import com.goji.salesforce.soap.SalesforceConnector;
import com.goji.salesforce.soap.enterprise.DocusignFlag__c;
import com.goji.salesforce.soap.enterprise.SObject;
import com.goji.salesforce.soap.enterprise.Lead;
import com.goji.salesforce.soap.enterprise.Opportunity;
import com.goji.salesforce.soap.enterprise.SaveResult;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import javax.xml.rpc.ServiceException;

/**
 *
 * @author paul.ye
 */
public class SalesforceAccess extends DefaultAccess {

    private SalesforceConnector salesforceConnector;
    
    private SalesforceConnector getConnector() {
        if (salesforceConnector == null) {
            salesforceConnector = new SalesforceConnector();
            try {
                //dev
                if(isTest){
                    salesforceConnector.login("api@consumerunited.com", "alOy(iL?CVpnTlmQh0f4\\?3AR^Zr", "BUir9Xf49o85AiDJFvcIGQ2i", "dev"); 
                } else {
                    salesforceConnector.login("api@consumerunited.com", "alOy(iL?CVpnTlmQh0f4\\?3AR^Zr", "BUir9Xf49o85AiDJFvcIGQ2i", null); 
                }
                //prod
                
            } catch (RemoteException ex) {
                LOG.severe("Cannot login salesfoce (Remote): " + ex.getMessage());
                return null;
            } catch (ServiceException ex) {
                LOG.severe("Cannot login salesfoce (Service): " + ex.getMessage());
                return null;
            }
        }
        return salesforceConnector;
    }

    public void updateDocuStatusByLeadEmail(String email, String status, String envelopId) throws RemoteException {
        if (salesforceConnector == null) {
            salesforceConnector = getConnector();
        }
        if (email == null || status == null || !email.matches("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$")) {
            LOG.severe("Invalid email address: " + email + " or status: " + status);
            return;
        }

        LOG.info("search : " + email);

        List<SObject> docuStatus = salesforceConnector.query("SELECT Id, Status__c, Lead__c, Opportunity__c, EnvelopId__c from DocusignFlag__c"
                + " where EnvelopId__c = '" + envelopId + "'");
        DocusignFlag__c docuObj = null;
        List<DocusignFlag__c> docuObjs = new ArrayList<DocusignFlag__c>();
        if (docuStatus == null || docuStatus.size() < 1) {
            docuObj = new DocusignFlag__c();
            docuObj.setStatus__c(status);
        } else {
            docuObj = (DocusignFlag__c) docuStatus.get(0);
            docuObj.setStatus__c(status);
        }
        List<SObject> sos = salesforceConnector.query("SELECT Id, Email__c from Opportunity where Email__c = '" + email + "'");
        Opportunity op = null;
        for (SObject obj : sos) {
            op = (Opportunity) obj;
            break;
        }
        if (op == null) {
            LOG.warning("No matched Opportunity for " + email);
        } else {
            docuObj.setOpportunity__c(op.getId());
            if (docuObj.getEnvelopId__c() == null) {
                docuObj.setEnvelopId__c(envelopId);
                docuObjs.add(docuObj);
                LOG.info("      inserting new flag to " + op.getId() + ", with envelopId: " + envelopId + ", | " + status);
                List<SaveResult> results = salesforceConnector.insert(docuObjs);
                for (SaveResult sr : results) {
                    if (!sr.isSuccess()) {
                        LOG.severe(sr.getErrors()[0].getMessage());
                    }
                }
            } else {
                docuObjs.add(docuObj);
                LOG.info("      updating flag to " + op.getId() + ", with envelopId: " + envelopId + ", | " + status);
                List<SaveResult> results = salesforceConnector.update(docuObjs);
                for (SaveResult sr : results) {
                    if (!sr.isSuccess()) {
                        LOG.severe(sr.getErrors()[0].getMessage());
                    }
                }
            }
            return;
        }

        sos = salesforceConnector.query("SELECT Id, Email from Lead where Email = '" + email + "'");
        Lead ld = null;
        for (SObject obj : sos) {
            ld = (Lead) obj;
            break;
        }
        if (ld == null) {
            LOG.warning("No matched Lead for " + email);
        } else {
            docuObj.setLead__c(ld.getId());
            if (docuObj.getEnvelopId__c() == null) {
                docuObj.setEnvelopId__c(envelopId);
                docuObjs.add(docuObj);
                LOG.info("      inserting new flag to " + ld.getId() + ", with envelopId: " + envelopId + ", | " + status);
                List<SaveResult> results = salesforceConnector.insert(docuObjs);
                for (SaveResult sr : results) {
                    if (!sr.isSuccess()) {
                        LOG.severe(sr.getErrors()[0].getMessage());
                    }
                }
            } else {
                docuObjs.add(docuObj);
                LOG.info("      updating flag to " + ld.getId() + ", with envelopId: " + envelopId + ", | " + status);
                List<SaveResult> results = salesforceConnector.update(docuObjs);
                for (SaveResult sr : results) {
                    if (!sr.isSuccess()) {
                        LOG.severe(sr.getErrors()[0].getMessage());
                    }
                }
            }
            return;
        }

    }
}
