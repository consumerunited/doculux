/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.goji.docusign.utl;

import com.goji.docusign.json.model.AuthenticationHeader;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.logging.Logger;

/**
 *
 * @author paul.ye
 */
public class ConnectionUtil {

    protected static final Logger LOG = Logger.getLogger(ConnectionUtil.class.getName());

    public HttpURLConnection authenticateLogin(AuthenticationHeader auth, String url, String method, String body) throws JsonProcessingException {
        HttpURLConnection conn = InitializeRequest(url, method, body, new ObjectMapper().writeValueAsString(auth));
        return conn;
    }

    private HttpURLConnection InitializeRequest(String url, String method, String body, String httpAuthHeader) {
//        ("___________________________________" + method + "_" + url + "________________________________________________________________________");
//        ("    [+] header : \n" + httpAuthHeader);
//        ("    [+] body : \n" + body);
        HttpURLConnection conn = null;
        try {
            conn = (HttpURLConnection) new URL(url).openConnection();

            conn.setRequestMethod(method);
            conn.setRequestProperty("X-DocuSign-Authentication", httpAuthHeader);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            if (method.equalsIgnoreCase("PUT") || method.equalsIgnoreCase("POST")|| method.equalsIgnoreCase("DELETE")) { //changed to PUT for test
                conn.setRequestProperty("Content-Length", Integer.toString(body.length()));
                conn.setDoOutput(true);
                DataOutputStream dos = new DataOutputStream(conn.getOutputStream());
                dos.writeBytes(body);
                dos.flush();

            } else if (method.equalsIgnoreCase("GET")) {
                //getting method
            }

            return conn;

        } catch (Exception e) {
            throw new RuntimeException(e); // simple exception handling, please review it
        }
    }

    public String getResponseBody(HttpURLConnection conn) {
        BufferedReader br = null;
        StringBuilder body = null;
        String line = "";
        try {
            // we use JSONPath to get the baseUrl and accountId from the JSON response body
            br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            body = new StringBuilder();
            while ((line = br.readLine()) != null) {
                body.append(line);
            }
            return body.toString();
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Response Body parsing Exception | " + e.getMessage()); // simple exception handling, please review it
        }
    }

    public void errorParse(HttpURLConnection conn, int status) {
        if (conn == null) {
            throw new RuntimeException("HttpURLConnection is null");
        }
        BufferedReader br;
        String line;
        StringBuilder responseError;
        try {
            //String debugthisshit = JsonPath.read(status, "$..");
            String errorResonse = "No Error Messages";
            if (conn.getErrorStream() != null) {
                InputStreamReader isr = new InputStreamReader(conn.getErrorStream());
                br = new BufferedReader(isr);
                responseError = new StringBuilder();
                line = null;
                while ((line = br.readLine()) != null) {
                    responseError.append(line);
                }
                errorResonse = responseError.toString();
            } 

            LOG.severe(errorResonse);
            return;
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e); // simple exception handling, please review it
        }
    }

}
