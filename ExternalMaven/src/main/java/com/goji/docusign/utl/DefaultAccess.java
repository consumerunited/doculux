/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.goji.docusign.utl;

import com.goji.docusign.json.model.AuthenticationHeader;
import com.fasterxml.jackson.core.JsonProcessingException;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author paul.ye
 */
public abstract class DefaultAccess {

    protected static final Logger LOG = Logger.getLogger(DefaultAccess.class.getName());

    protected static final String SERVER = "https://demo.docusign.net";
    protected static final String SERVER_PROD = "https://na2.docusign.net";
    protected static final String REST_API = "/restapi/v2";
    protected static final String LOGIN = "/login_information";

    protected static final String PROD_ACCOUNT_ID = "15402858";
    protected static final String DEV_ACCOUNT_ID = "882580";
    protected static final String DEV_API_KEY = "GOJI-9cc29155-1367-4bcc-8766-b2fc94637971";

    protected static final ConnectionUtil util = new ConnectionUtil();

    protected boolean isTest;

    public void login(AuthenticationHeader auth) throws JsonProcessingException, IOException {

        String accessServer = isTest ? SERVER : SERVER_PROD;
        LOG.info("Login into : " + auth.getUsername() + " by " + auth.getPassword() + " using " + auth.getIntegratorKey() + " @: " + accessServer + REST_API + LOGIN);
        HttpURLConnection conn = util.authenticateLogin(auth, accessServer + REST_API + LOGIN, "GET", "");
        int status = conn.getResponseCode();
        
        if (status != 200) {
            LOG.severe("login request failed! [" + status + "], " + conn.getResponseMessage() + " | " + conn.getResponseCode());
            return;
        }

    }

    protected String restCall(AuthenticationHeader auth, String url, String method, String body) throws JsonProcessingException {
        String accessServer = isTest ? SERVER : SERVER_PROD;
        HttpURLConnection conn = util.authenticateLogin(auth, accessServer + REST_API + url, method, body);
        int status = -1;
        try {
            status = conn.getResponseCode();
        } catch (IOException ex) {
            LOG.log(Level.SEVERE, "REST Call Error, no response body parsed.", ex);
        }
        if (status != 200) {
            util.errorParse(conn, status);
        }
        String response = util.getResponseBody(conn);
        return response;
    }

    public boolean isIsTest() {
        return isTest;
    }

    public void setIsTest(boolean isTest) {
        this.isTest = isTest;
    }
}
