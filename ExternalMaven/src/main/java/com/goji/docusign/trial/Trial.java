/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.goji.docusign.trial;

import com.goji.docusign.DocuSignClient;
import com.goji.docusign.json.Document;
import com.goji.docusign.json.Recipients;
import com.goji.docusign.json.RequestSignatureFromDocuments;
import com.goji.docusign.json.SignHereTab;
import com.goji.docusign.json.Signer;
import com.goji.docusign.json.Tabs;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author paul.ye
 */
public class Trial {

    public static void main(String[] args) throws IOException {
        Trial t = new Trial();
        DocuSignClient c = t.getClient();
        
        System.out.println(t.sendEnvelope(c));
    }

    private DocuSignClient getClient() throws IOException {
        DocuSignClient c = new DocuSignClient("paul.ye@goji.com", "Goji222015", "DOCU-0ff5939e-0386-4751-bb89-d8fce8c99f9b");
        if (c.login()) {
            System.out.println(c.getLastResponseText());
        }
        return c;
    }

    private String sendEnvelope(DocuSignClient c) throws IOException {
        // Create new signature request object
        RequestSignatureFromDocuments request = new RequestSignatureFromDocuments();
// create one recipient of type signer
        Signer signer = new Signer();
        signer.setEmail("paul.ye@goji.com");
        signer.setName("paul test");
        signer.setRecipientId("1");
        List signers = Arrays.asList(signer);
        Recipients recipients = new Recipients();
        recipients.setSigners(signers);

// place signature tab 100 pixels right and 150 pixels down from top left of page
        SignHereTab tab1 = new SignHereTab();
        tab1.setDocumentId("1");
        tab1.setPageNumber("1");
        tab1.setXPosition("100");
        tab1.setYPosition("200");
        List<SignHereTab> signatureTabs = Arrays.asList(tab1);

// assign the tab to the signer
        Tabs tabs = new Tabs();
        tabs.setSignHereTabs(signatureTabs);
        signer.setTabs(tabs);

// set document info
        Document document = new Document();
        document.setName("TEST.PDF");
        document.setDocumentId("1");
        List documents = Arrays.asList(document);

// configure the signature request object
        request.setRecipients(recipients);
        request.setDocuments(documents);
        request.setEmailSubject("DocuSign Test");

// "sent" to send request immediately, "created" to save as draft
        request.setStatus("sent");

        File testFile = new File("tests/picturePdf.PDF");
        File testFile_table = new File("TEST.PDF");
        File[] files = new File[]{testFile_table};

//*** Create & send the envelope
        return c.reqeustSignatureFromDocuments(request, files);
    }
    
    

}
