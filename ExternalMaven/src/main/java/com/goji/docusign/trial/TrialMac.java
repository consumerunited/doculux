/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.goji.docusign.trial;

import com.goji.docusign.json.model.AuthenticationHeader;
import com.goji.docusign.json.model.Folder;
import com.goji.docusign.json.model.UserInfo;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author paul.ye
 */
public class TrialMac {

    private static final String lurl = "https://demo.docusign.net/restapi/v2/login_information";
    private static final Logger LOG = Logger.getLogger(TrialMac.class.getName());

    private static final String integratorKey = "GOJI-9cc29155-1367-4bcc-8766-b2fc94637971";			// integrator key (found on Preferences -> API page)	
    private static final String username = "mackenzie.allen@goji.com";			// account email (or your API userId)
    private static final String password = "minimed1!2@3#4$5%";			// account Password
    


    private static final String integratorKey2 = "GOJI-9cc29155-1367-4bcc-8766-b2fc94637971";			// integrator key (found on Preferences -> API page)	
    private static final String username2 = "paul.ye@goji.com";			// account email (or your API userId)
    private static final String password2 = "Goji222015";			// account Password
    private static final String userId = "840cd75c-a8f8-40f8-a7fd-47952bed1bc5";

    private static final String server = "https://demo.docusign.net";
    private static final String restApi = "/restapi/v2";

    public static void main(String[] args) throws IOException {
        String testFolder = "{   \"ownerUserName\":\"Mackenzie Allen\",     \"ownerEmail\":\"mackenzie.allen@goji.com\", "
                + "       \"ownerUserId\":\"9844b818-5e92-48c4-97a5-21cdb76a36df\",       \"type\":\"inbox\",        \"name\":\"Inbox\",     \"uri\":\"/folders/108a09b1-4f4c-4ff2-a337-70e60573d6e1\",       \"folderId\":\"108a09b1-4f4c-4ff2-a337-70e60573d6e1\"   }";
        Folder f = new ObjectMapper().readValue(testFolder, Folder.class);
        System.out.println(f);

        TrialMac m = new TrialMac();
        UserInfo ui = m.authenticateLogin();
        
    }

    public UserInfo authenticateLogin() throws JsonProcessingException, IOException {

        AuthenticationHeader auth = new AuthenticationHeader();
        auth.setIntegratorKey(integratorKey);
        auth.setPassword(password);
        auth.setUsername(username);

        AuthenticationHeader auth2 = new AuthenticationHeader();
        auth2.setIntegratorKey(integratorKey2);
        auth2.setUsername(username2);
        auth2.setPassword(password2);

        HttpURLConnection conn = InitializeRequest(lurl, "GET", "", new ObjectMapper().writeValueAsString(auth));

        int status = conn.getResponseCode();
        if (status != 200) {
            LOG.severe("init request failed!");
            return null;
        }

        String responseStr = getResponseBody(conn);
        LOG.info("response: \n" + responseStr);

        UserInfo ui = new UserInfo();
        ui.setAccountId((String) JsonPath.read(responseStr, "$.loginAccounts.[0].accountId"));
        ui.setBaseURL((String) JsonPath.read(responseStr, "$.loginAccounts.[0].baseUrl"));

        List<String> envelopes = getEnvelopes(auth, ui);
        searchFolders(auth, ui);
        getFolderList(auth, ui);
        return ui;
    }

    public HttpURLConnection InitializeRequest(String url, String method, String body, String httpAuthHeader) {
        //LOG.info("auth : " + httpAuthHeader);
        HttpURLConnection conn = null;
        try {
            conn = (HttpURLConnection) new URL(url).openConnection();

            conn.setRequestMethod(method);
            conn.setRequestProperty("X-DocuSign-Authentication", httpAuthHeader);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            if (method.equalsIgnoreCase("PUT")) { //changed to PUT for test
                conn.setRequestProperty("Content-Length", Integer.toString(body.length()));
                conn.setDoOutput(true);
                DataOutputStream dos = new DataOutputStream(conn.getOutputStream());
                dos.writeBytes(body);
                dos.flush();

            } else if (method.equalsIgnoreCase("GET")) {
                //getting method
            }

            return conn;

        } catch (Exception e) {
            throw new RuntimeException(e); // simple exception handling, please review it
        }
    }

    public String getResponseBody(HttpURLConnection conn) {
        BufferedReader br = null;
        StringBuilder body = null;
        String line = "";
        try {
            // we use JSONPath to get the baseUrl and accountId from the JSON response body
            br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            body = new StringBuilder();
            while ((line = br.readLine()) != null) {
                body.append(line);
            }
            return body.toString();
        } catch (Exception e) {
            throw new RuntimeException(e); // simple exception handling, please review it
        }
    }

    public void errorParse(HttpURLConnection conn, int status) {
        BufferedReader br;
        String line;
        StringBuilder responseError;
        try {
            System.out.print("API call failed, status returned was: " + status);
            System.out.println(status); //test debug message
            //String debugthisshit = JsonPath.read(status, "$..");
            InputStreamReader isr = new InputStreamReader(conn.getErrorStream());
            br = new BufferedReader(isr);
            responseError = new StringBuilder();
            line = null;
            while ((line = br.readLine()) != null) {
                responseError.append(line);
            }

            String errorResonse = responseError.toString();
            System.out.println(errorResonse);
            return;
        } catch (Exception e) {
            throw new RuntimeException(e); // simple exception handling, please review it
        }
    }

    public List<String> getEnvelopes(AuthenticationHeader auth, UserInfo ui) throws JsonProcessingException //returns list of envelopes
    {

        List<String> envelopes = null;
        String totalSetSize = "";
        String body = "";
        String month, day, year;
        month = "3";
        day = "30";
        year = "2015";
        //String qParam1 = "?from_date=" + month + "%2F" + day + "%2F" + year;  // url encoded
        //String qParam2 = "&status=created";
        String qParam1 = "?from_date=" + month + "%2F" + day + "%2F" + year;  // url encoded
        String qParam2 = "&status=created";
        //String qParam3 = "&custom_field=policynum";

        //url = baseURL + "/envelopes/" + qParam1 + qParam2;
        String url = ui.getBaseURL() + "/folders/df285a89-0f2d-439f-b264-5cd448c6712f/" + qParam1 + qParam2;
        HttpURLConnection conn = InitializeRequest(url, "GET", "", new ObjectMapper().writeValueAsString(auth));

        int status = -1;
        try {
            status = conn.getResponseCode();
        } catch (IOException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
        if (status != 200) // 200 = OK
        {
            errorParse(conn, status);
            //return;
        }
        String response = getResponseBody(conn);
        System.out.println("ENV Resp: \n" + response);
        //System.out.println(response);
        //List<String> envelopes = JsonPath.read(response, "$..envelopes.envelopeId");
//        envelopes = JsonPath.read(response, "$..folderItems.envelopeId");
        //JSONObject envelopes = new JSONObject();
//        System.out.println(envelopes);

        return envelopes;
    }

    private void getFolderList(AuthenticationHeader auth, UserInfo ui) throws JsonProcessingException, IOException {
        System.out.println("Folder List: _____________________________________________________");
        String url = ui.getBaseURL() + "/folders";
        String resp = restCall(auth, url);
        System.out.println("Resp: \n" + resp);
//        ObjectMapper mapper = new ObjectMapper();
//        Folders f = mapper.readValue(resp, Folders.class);
//        if (f != null) {
//            System.out.println(f);
//        }
    }

    private void searchFolders(AuthenticationHeader auth, UserInfo ui) throws JsonProcessingException, IOException {
        System.out.println("Draft Folder: _____________________________________________________");
        String urlDraft = server + restApi + "/accounts/" + ui.getAccountId() + "/search_folders/Drafts";
        String resp = restCall(auth, urlDraft);
        System.out.println("Resp: \n" + resp);
        
    }

    private String restCall(AuthenticationHeader auth, String url) throws JsonProcessingException {
        HttpURLConnection conn = InitializeRequest(url, "GET", "", new ObjectMapper().writeValueAsString(auth));
        int status = -1;
        try {
            status = conn.getResponseCode();
        } catch (IOException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
        if (status != 200) {
            errorParse(conn, status);
        }
        String response = getResponseBody(conn);
        return response;
    }
}
