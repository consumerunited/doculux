/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.goji.docusign.trial;

import java.util.*;
import java.io.*;
import java.sql.*;
import java.net.*;

//import com.sun.xml.internal.messaging.saaj.soap.Envelope;
import com.jayway.jsonpath.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author edwin.yip
 */
class DocusignEnvelope {

    private static final String lurl = "https://demo.docusign.net/restapi/v2/login_information";
    private static String url = "";			// end-point for each api call
//    private static ArrayList<Envelope> envelopeList = new ArrayList<Envelope>();
    private static final String integratorKey = "\"GOJI-9cc29155-1367-4bcc-8766-b2fc94637971\"";			// integrator key (found on Preferences -> API page)	
    private static final String username = "\"mackenzie.allen@goji.com\"";			// account email (or your API userId)
    private static final String password = "\"minimed1!2@3#4$5%\"";			// account password
    private static String signer = "";
    private static String recipientId = "";

    private static String pNewRoutingOrder = "";
    private static String baseURL;
    private static String accountId;
    private static final String authenticationHeader = "{\"Username\":" + username + ", " + "\"Password\":" + password + ", "
            + "\"IntegratorKey\":" + integratorKey + "}";
    private static String updateRecipentInfo;

    private static Connection leads_db = null;
    private static Connection internal_db = null;
    private static String dev_prod = "";
    ;
    private static final Double LeadtoContact = 0.50;
    private static final Integer LeadsPerAgent = 60;
    //private static List<Burners> burnersList = new ArrayList<Burners>();
    //private static Burners burners;
    private static final Integer onTime = 8;
    private static final String policyNumber = "X5850960";

    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException {
        dev_prod = "staging";
        authenticateLogin();
        reconnectToMySQL();
        List<String> envelopes = getEnvelopes();
        Map<String, String> customFields = getCustomFieldEnvelope(envelopes);
        Map<String, List<String>> policyHolderInfo = getPolicyholderInfo(customFields);
        updateRecipients(policyHolderInfo);
        sendDraftEnvelopes(policyHolderInfo);
        try {
            internal_db.close();
            //leads_db.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }

    } //end main()

    public static void authenticateLogin() {
        // construct the DocuSign authentication header

        // additional variable declarations
        baseURL = "";			// we will retrieve this through the Login API call
        accountId = "";			// we will retrieve this through the Login API call

        String body = "";			// request body
        String response = "";			// response body
        int status;				// response status

        //============================================================================
        // STEP 1 - Make the Login API call to retrieve your baseUrl and accountId
        //============================================================================
        body = "";	// no request body for the login call
        // create connection object, set request method, add request headers
        HttpURLConnection conn = InitializeRequest(lurl, "GET", body, authenticationHeader);

        // send the request
        System.out.println("Step 1:  Sending Login request...\n");
        status = -1;
        try {
            status = conn.getResponseCode();
        } catch (IOException ex) {
            Logger.getLogger(DocusignEnvelope.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (status != 200) // 200 = OK
        {
            errorParse(conn, status);
            return;
        }

        // obtain baseUrl and accountId values from response body 
        response = getResponseBody(conn);
        //System.out.println(response);
        baseURL = JsonPath.read(response, "$.loginAccounts.[0].baseUrl");
        accountId = JsonPath.read(response, "$.loginAccounts.[0].accountId");
        System.out.println("Base URL: " + baseURL);
        System.out.println("AccountID: " + accountId);

    }

    public static String getResponseBody(HttpURLConnection conn) {
        BufferedReader br = null;
        StringBuilder body = null;
        String line = "";
        try {
            // we use JSONPath to get the baseUrl and accountId from the JSON response body
            br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            body = new StringBuilder();
            while ((line = br.readLine()) != null) {
                body.append(line);
            }
            return body.toString();
        } catch (Exception e) {
            throw new RuntimeException(e); // simple exception handling, please review it
        }
    }

    public static HttpURLConnection InitializeRequest(String url, String method, String body, String httpAuthHeader) {
        HttpURLConnection conn = null;
        try {
            conn = (HttpURLConnection) new URL(url).openConnection();

            conn.setRequestMethod(method);
            conn.setRequestProperty("X-DocuSign-Authentication", httpAuthHeader);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            if (method.equalsIgnoreCase("PUT")) { //changed to PUT for test
                conn.setRequestProperty("Content-Length", Integer.toString(body.length()));
                conn.setDoOutput(true);
                DataOutputStream dos = new DataOutputStream(conn.getOutputStream());

                dos.writeBytes(body);
                dos.flush();

            } else if (method.equalsIgnoreCase("GET")) {

            }

            return conn;

        } catch (Exception e) {
            throw new RuntimeException(e); // simple exception handling, please review it
        }
    }

    public static void errorParse(HttpURLConnection conn, int status) {
        BufferedReader br;
        String line;
        StringBuilder responseError;
        try {
            System.out.print("API call failed, status returned was: " + status);
            System.out.println(status); //test debug message
            //String debugthisshit = JsonPath.read(status, "$..");
            InputStreamReader isr = new InputStreamReader(conn.getErrorStream());
            br = new BufferedReader(isr);
            responseError = new StringBuilder();
            line = null;
            while ((line = br.readLine()) != null) {
                responseError.append(line);
            }

            String errorResonse = responseError.toString();
            System.out.println(errorResonse);
            return;
        } catch (Exception e) {
            throw new RuntimeException(e); // simple exception handling, please review it
        }
    }

    public static void updateRecipients(Map userInfo) {
        Iterator it = userInfo.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            //String body = "{\"signers\":{\"" + signer + ":{\"recipientId\":\"" + recipientId + ",\"email\":\"" + pNewEmail + ",\"name\":\"" + pNewName + ",\"routingOrder\":\"" + pNewRoutingOrder + "\"}}}";
            String pNewEmail = ((List<String>) pair.getValue()).get(2);
            String pNewName = ((List<String>) pair.getValue()).get(0) + " " + ((List<String>) pair.getValue()).get(1);
            String body = "{\"signers\":[{\"email\":\"" + pNewEmail + "\",\"name\":\"" + pNewName + "\",\"recipientId\":\"1\"}]}";
            System.out.println(body);
            String month, day, year;
            String envelopeID;
            envelopeID = (String) pair.getKey();

            // append "/envelopes/{envelopeId}" plus optional from_date and status query params to baseUrl and use in request
            url = baseURL + "/envelopes/" + envelopeID + "/recipients" + "?resend_envelope=true";

            HttpURLConnection conn = InitializeRequest(url, "PUT", body, authenticationHeader);

            int status = -1;
            try {
                status = conn.getResponseCode();
            } catch (IOException ex) {
                Logger.getLogger(DocusignEnvelope.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (status != 200) // 200 = OK
            {
                errorParse(conn, status);
                //return;
            }
            String response = getResponseBody(conn);
            System.out.println(response);
        }
    }

    public static void sendDraftEnvelopes(Map userInfo) {
        Iterator it = userInfo.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            //String body = "{\"signers\":{\"" + signer + ":{\"recipientId\":\"" + recipientId + ",\"email\":\"" + pNewEmail + ",\"name\":\"" + pNewName + ",\"routingOrder\":\"" + pNewRoutingOrder + "\"}}}";
            //String pNewEmail = ((List<String>)pair.getValue()).get(2);
            //String pNewName = ((List<String>)pair.getValue()).get(0) + " " + ((List<String>)pair.getValue()).get(1);
            String body = "{\"status\":\"sent\"}";
            System.out.println(body);
            String envelopeID;
            envelopeID = (String) pair.getKey();

            // append "/envelopes/{envelopeId}" plus optional from_date and status query params to baseUrl and use in request
            url = baseURL + "/envelopes/" + envelopeID;

            HttpURLConnection conn = InitializeRequest(url, "PUT", body, authenticationHeader);

            int status = -1;
            try {
                status = conn.getResponseCode();
            } catch (IOException ex) {
                Logger.getLogger(DocusignEnvelope.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (status != 200) // 200 = OK
            {
                errorParse(conn, status);
                //return;
            }
            String response = getResponseBody(conn);
            System.out.println(response);
        }

    }

    // Must change this to be dynamic and use values set outside of the program.
    public static void createEnvelope(String body) {

        String month, day, year, allowMarkup, allowReassign, allowRecipientRecursion, asynchronous, authoritativeCopy, autoNavigation, brandId, emailBlurb, emailSubject, enableWetSign, enforceSignerVisibility, envelopeIdStamping, messageLock, notification, recipientsLock, signingLocation, status, transactionId, useDisclosure, customFields, documents, recipients, eventNotification, emailSettings;
        month = "3";
        day = "6";
        year = "2015";

        HttpURLConnection conn = InitializeRequest(lurl, "GET", body, authenticationHeader);

        String qParam1 = "?from_date=" + month + "%2F" + day + "%2F" + year;  // url encoded
        String qParam2 = "&status=created";

        // append "/envelopes/{envelopeId}" plus optional from_date and status query params to baseUrl and use in request
        url = baseURL + "/envelopes" + qParam1 + qParam2;

    }

    public static void sendEnvelope() {

        String month, day, year;
        month = "3";
        day = "6";
        year = "2015";
        String qParam1 = "?from_date=" + month + "%2F" + day + "%2F" + year;  // url encoded
        String qParam2 = "&status=created";

        // append "/envelopes/{envelopeId}" plus optional from_date and status query params to baseUrl and use in request
        url = baseURL + "/envelopes" + qParam1 + qParam2;

    }

    public static List<String> getEnvelopes() //returns list of envelopes
    {
        String totalSetSize = "";
        String body = "";
        String month, day, year;
        month = "3";
        day = "30";
        year = "2015";
        //String qParam1 = "?from_date=" + month + "%2F" + day + "%2F" + year;  // url encoded
        //String qParam2 = "&status=created";
        String qParam1 = "?from_date=" + month + "%2F" + day + "%2F" + year;  // url encoded
        String qParam2 = "&status=created";
        //String qParam3 = "&custom_field=policynum";

        //url = baseURL + "/envelopes/" + qParam1 + qParam2;
        url = baseURL + "/folders/df285a89-0f2d-439f-b264-5cd448c6712f/" + qParam1 + qParam2;

        HttpURLConnection conn = InitializeRequest(url, "GET", body, authenticationHeader);

        int status = -1;
        try {
            status = conn.getResponseCode();
        } catch (IOException ex) {
            Logger.getLogger(DocusignEnvelope.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (status != 200) // 200 = OK
        {
            errorParse(conn, status);
            //return;
        }
        String response = getResponseBody(conn);
        //System.out.println(response);
        //List<String> envelopes = JsonPath.read(response, "$..envelopes.envelopeId");
        List<String> envelopes = JsonPath.read(response, "$..folderItems.envelopeId");
        //JSONObject envelopes = new JSONObject();
        System.out.println(envelopes);

        return envelopes;
    }

    public static Map<String, String> getCustomFieldEnvelope(List<String> envelopes) {
        Map<String, String> envelopeToPolicy = new HashMap();
        for (String envelope : envelopes) {
            JsonPath.parse(envelopes);
            //System.out.println(envelopes);
            String qParam2 = "/custom_fields/";
            url = baseURL + "/envelopes/" + envelope + qParam2;
            String body = "";
            HttpURLConnection conn = InitializeRequest(url, "GET", body, authenticationHeader);
            //String qParam3 = "&custom_field=" + policyNumber ;
            System.out.println(url);
            int status = -1;
            try {
                status = conn.getResponseCode();
            } catch (IOException ex) {
                Logger.getLogger(DocusignEnvelope.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (status != 200) // 200 = OK
            {
                errorParse(conn, status);
            }
            String response = getResponseBody(conn);
            List<String> customFields = JsonPath.read(response, "$..listCustomFields.value");
            if (customFields.size() > 0) {
                envelopeToPolicy.put(envelope, customFields.get(0));
            }
        }
        return envelopeToPolicy;
    }

    public static void forceCustomField() {

    }

    public static void getSFDCEnvelope() {

    }

    public static void updateSFDCEnvelope() {

    }

    public static void getSFDCRecipient() {

    }

    public static void updateSFDCRecipient() {

    }

    private static void reconnectToMySQL() {

//        switch (dev_prod) {
//            case "dev":
//                //Establish a connection to dev-db.consumerunited.com, a simulation of leads-db
//                leads_db = new MySQLDatabase(
//                        "jdbc:mysql://10.0.1.88:3306/eric_unit_calls_database",
//                        "dev", "186South").getConnection();
//                break;
//            case "prod":
//                //Establish a connection to leads-db PRODUCTION
//                leads_db = new MySQLDatabase(
//                        "jdbc:mysql://leads-db.cpzuqtj4hq4e.us-east-1.rds.amazonaws.com:3306/calls_database",
//                        "dev", "186South").getConnection();
//                break;
//            case "staging":
//                internal_db = new MySQLDatabase(
//                        "jdbc:mysql://policy-dev.cpzuqtj4hq4e.us-east-1.rds.amazonaws.com/consumer_united",
//                        "root", "Arlington977").getConnection();
//                break;
//            default:
//                System.out.println("To connect to the leads_db, dev_prod variable "
//                        + "needs to be 'dev' or 'prod', Stop.");
//                System.exit(1);
//
//
//        }
//        System.out.println("connection established");
    }

    public static Map<String, List<String>> getPolicyholderInfo(Map envelopeToPolicy) {
        String query = new String();
        Map<String, List<String>> userInfo = new HashMap();
        Iterator it = envelopeToPolicy.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            ResultSet rs = null;
            query = "SELECT `lead_id`, `email`, `driver1_first_name`, `driver1_last_name` FROM `qa` WHERE policy_number='" + (String) pair.getValue() + "'";
            try {
                //PreparedStatement stmt = internal_db.prepareStatement(query);
                Statement stmt = internal_db.createStatement();
                rs = stmt.executeQuery(query);
                while (rs.next()) {
                    List<String> info = new ArrayList();
                    info.add(rs.getString("driver1_first_name"));
                    info.add(rs.getString("driver1_last_name"));
                    info.add(rs.getString("email"));
                    userInfo.put((String) pair.getKey(), info);
                    break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return userInfo;
    }

    /*public String httpPostData(String postData) throws IOException {
     // curl_init and url
     URL url = new URL("http://some.host.com/somewhere/to/");
     HttpURLConnection con = (HttpURLConnection) url.openConnection();

     //  CURLOPT_POST
     con.setRequestMethod("POST");

     // CURLOPT_FOLLOWLOCATION
     con.setInstanceFollowRedirects(true);

     con.setRequestProperty("Content-length", String.valueOf(postData.length()));

     con.setDoOutput(true);
     con.setDoInput(true);

     DataOutputStream output = new DataOutputStream(con.getOutputStream());
     output.writeBytes(postData);
     output.close();

     // "Post data send ... waiting for reply");
     int code = con.getResponseCode(); // 200 = HTTP_OK
     System.out.println("Response    (Code):" + code);
     System.out.println("Response (Message):" + con.getResponseMessage());

     // read the response
     DataInputStream input = new DataInputStream(con.getInputStream());
     int c;
     StringBuilder resultBuf = new StringBuilder();
     while ( (c = input.read()) != -1) {
     resultBuf.append((char) c);
     }
     input.close();

     return resultBuf.toString();
     }*/

    /*public static Map<String, String> getEnvelopeStatus(List<String> envelopes) {
     Map<String, String> envStatuses = new HashMap();
     Map<String, String> envelopeStatusToPolicy;
     for(String envelope : envelopes) {
     JsonPath.parse(envelopes);
     url = baseURL + "/envelopes/" + envelope ;
     String body = "";
     HttpURLConnection conn = InitializeRequest(url, "GET", body, authenticationHeader);
     System.out.println(url);
     int status = -1;
     try {
     status = conn.getResponseCode();
     } catch (IOException ex) {
     Logger.getLogger(DocusignEnvelope.class.getName()).log(Level.SEVERE, null, ex);
     }
     if (status != 200) // 200 = OK
     {
     errorParse(conn, status);
     }
     String response = getResponseBody(conn);
     List<String> envStatuses = JsonPath.read(response, "$..folderItems.status");
     if(envStatuses.size() > 0)
     envelopeStatusToPolicy.put(envelope, envStatuses.get(0));
     }
     return envelopeStatusToPolicy;
     }*/
}
