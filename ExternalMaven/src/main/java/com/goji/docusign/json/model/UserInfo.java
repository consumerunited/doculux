/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.goji.docusign.json.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author paul.ye
 */
public class UserInfo {

    @JsonProperty
    private String baseURL;
    @JsonProperty
    private String accountId;

    public String getBaseURL() {
        return baseURL;
    }

    public void setBaseURL(String baseURL) {
        this.baseURL = baseURL;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    @Override
    public String toString() {
        return "[" + accountId + "] " + baseURL;
    }
}
