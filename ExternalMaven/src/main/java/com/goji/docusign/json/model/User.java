/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.goji.docusign.json.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author paul.ye
 */
@JsonInclude(Include.NON_NULL)
public class User {
    
    private List<Group> groupList; //groupId, groupName // pull from group information, should be none, we can handle this manually later
    
    private String email;
    private String userName;
    private String firstName;
    private String lastName;
    private String middleName;
    private String password;
    private String activationAccessCode;
    private ForgottenPasswordInfo forgottenPasswordInfo;
    
    private String userId; //null on create, for mapper read.
    private String userType;
    private String userStatus;
    private String uri;
    private String createdDateTime;
    
    private List<UserSettingsItem> userSettings; //self initialized.
    
    /** Non-required fields
     * private final UserSettings userSettings = new UserSettings(); Not required
    private final String suffixName = "";
    private final String title = "";
    * private String forgottenPasswordInfo;
    * private String enableConnectForUser = "false"; 
    * private String sendActivationOnInvalidLogin; //"false"
     */
    
    public static User newAgent(String email, String password, String firstName, String lastName, String middleName, List<Group> groupList, ForgottenPasswordInfo defaultInfo ){
        User u = new User();
        u.setEmail(email);
        u.setActivationAccessCode(email);
        u.setFirstName(firstName);
        u.setLastName(lastName);
        u.setMiddleName(middleName);
        u.setUserName(firstName + " " + middleName + " " + lastName);
        u.setGroupList(groupList);
//        u.setPassword(password);
        //doesn't set the password
        u.setForgottenPasswordInfo(defaultInfo);
        
        u.setUserSettings(new ArrayList<UserSettingsItem>());
        List<UserSettingsItem> stList = u.getUserSettings();
        
        stList.add(new UserSettingsStringItem("canManageAccount",DefaultUserSettings.canManageAccount.toString()));
        stList.add(new UserSettingsStringItem("canSendEnvelope",DefaultUserSettings.canSendEnvelope.toString()));
        stList.add(new UserSettingsStringItem("locale",DefaultUserSettings.locale.toString()));
        stList.add(new UserSettingsStringItem("canSendAPIRequests",DefaultUserSettings.canSendAPIRequests.toString()));
        stList.add(new UserSettingsStringItem("apiAccountWideAccess",DefaultUserSettings.apiAccountWideAccess.toString()));
        stList.add(new UserSettingsStringItem("enableVaulting",DefaultUserSettings.enableVaulting.toString()));
        stList.add(new UserSettingsStringItem("vaultingMode",DefaultUserSettings.vaultingMode.toString()));
        stList.add(new UserSettingsStringItem("enableTransactionPoint",DefaultUserSettings.enableTransactionPoint.toString()));
        stList.add(new UserSettingsStringItem("enableSequentialSigningAPI",DefaultUserSettings.enableSequentialSigningAPI.toString()));
        stList.add(new UserSettingsStringItem("enableSquentialSigningUI",DefaultUserSettings.enableSquentialSigningUI.toString()));
        stList.add(new UserSettingsStringItem("enableDSPro",DefaultUserSettings.enableDSPro.toString()));
        stList.add(new UserSettingsStringItem("powerFormAdmin",DefaultUserSettings.powerFormAdmin.toString()));
        stList.add(new UserSettingsStringItem("powerFormUser",DefaultUserSettings.powerFormUser.toString()));
        stList.add(new UserSettingsStringItem("canEditSharedAddressbook",DefaultUserSettings.canEditSharedAddressbook.toString()));
        stList.add(new UserSettingsStringItem("canManageTemplates",DefaultUserSettings.canManageTemplates.toString()));
        stList.add(new UserSettingsStringItem("enableSignOnPaperOverride",DefaultUserSettings.enableSignOnPaperOverride.toString()));
        stList.add(new UserSettingsStringItem("enableSignerAttachments",DefaultUserSettings.enableSignerAttachments.toString()));
        stList.add(new UserSettingsStringItem("allowSendOnBehalfOf",DefaultUserSettings.allowSendOnBehalfOf.toString()));
        stList.add(new UserSettingsStringItem("allowRecipientLanguageSelection",DefaultUserSettings.allowRecipientLanguageSelection.toString()));
        stList.add(new UserSettingsStringItem("canLockEnvelopes",DefaultUserSettings.canLockEnvelopes.toString()));
        stList.add(new UserSettingsStringItem("canUseScratchpad",DefaultUserSettings.canUseScratchpad.toString()));
        stList.add(new UserSettingsStringItem("canCreateWorkspaces",DefaultUserSettings.canCreateWorkspaces.toString()));
        stList.add(new UserSettingsStringItem("allowEmailChange",DefaultUserSettings.allowEmailChange.toString()));
        stList.add(new UserSettingsStringItem("allowPasswordChange",DefaultUserSettings.allowPasswordChange.toString()));
        stList.add(new UserSettingsStringItem("bulksend",DefaultUserSettings.bulksend.toString()));
        stList.add(new UserSettingsStringItem("selfSignedRecipientEmailDocument",DefaultUserSettings.selfSignedRecipientEmailDocument.toString()));

        return u;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getActivationAccessCode() {
        return activationAccessCode;
    }

    public void setActivationAccessCode(String activationAccessCode) {
        this.activationAccessCode = activationAccessCode;
    }
    
    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("  {\n");
        sb.append("     ").append("userName : ").append(this.userName).append(", \n");
        sb.append("     ").append("email : ").append(this.email).append(", \n");
        sb.append("     ").append("password : ").append(this.password).append(", \n");
        sb.append("     ").append("groupList : ").append(this.getGroupList()).append(", \n");
        
        sb.append("     ").append("userId : ").append(this.userId).append(", \n");
        sb.append("     ").append("userType : ").append(this.userType).append(", \n");
        sb.append("     ").append("userStatus : ").append(this.userStatus).append(", \n");
        sb.append("     ").append("uri : ").append(this.uri).append(", \n");
        sb.append("     ").append("createdDateTime : ").append(this.createdDateTime).append(", \n");
  
        /**
         * Cannot retrieve settings in the normal user object. Need a full detail user object to call and map.
         */
//        if(userSettings!=null && userSettings.size()>0){
//            sb.append("     ").append("userSettings : ").append("[\n");
//            for(UserSettingsItem usti : userSettings){
//                sb.append("     ").append(usti);
//            }
//            sb.append("     ").append("]\n");
//        }
        
        sb.append("  }\n");
        return sb.toString();
    }

    public List<Group> getGroupList() {
        return groupList;
    }

    public void setGroupList(List<Group> groupList) {
        this.groupList = groupList;
    }

    public ForgottenPasswordInfo getForgottenPasswordInfo() {
        return forgottenPasswordInfo;
    }

    public void setForgottenPasswordInfo(ForgottenPasswordInfo forgottenPasswordInfo) {
        this.forgottenPasswordInfo = forgottenPasswordInfo;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(String createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public List<UserSettingsItem> getUserSettings() {
        return userSettings;
    }

    public void setUserSettings(List<UserSettingsItem> userSettings) {
        this.userSettings = userSettings;
    }

}
