/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.goji.docusign.json.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author paul.ye
 */
public class AuthenticationHeader {

    @JsonProperty
    private String Username;
    @JsonProperty
    private String Password;
    @JsonProperty
    private String IntegratorKey;

    public String getUsername() {
        return Username;
    }

    public void setUsername(String username) {
        this.Username = username;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        this.Password = password;
    }

    public String getIntegratorKey() {
        return IntegratorKey;
    }

    public void setIntegratorKey(String integratorKey) {
        this.IntegratorKey = integratorKey;
    }

}
