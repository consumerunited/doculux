/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.goji.docusign.json.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author paul.ye
 */
@JsonInclude(Include.NON_NULL)
public class Group {
    @JsonProperty
    private String groupId;
    @JsonProperty
    private String groupName;
    @JsonProperty
    private String groupType;
    @JsonProperty
    private String permissionProfileId;

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupType() {
        return groupType;
    }

    public void setGroupType(String groupType) {
        this.groupType = groupType;
    }

    public String getPermissionProfileId() {
        return permissionProfileId;
    }

    public void setPermissionProfileId(String permissionProfileId) {
        this.permissionProfileId = permissionProfileId;
    }

    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("  {\n");
        sb.append("     ").append("groupId : ").append(this.groupId).append(", \n");
        sb.append("     ").append("groupName : ").append(this.groupName).append(", \n");
        sb.append("     ").append("permissionProfileId : ").append(this.permissionProfileId).append(", \n");
        sb.append("     ").append("groupType : ").append(this.groupType).append(", \n");
        sb.append("  }\n");
        return sb.toString();
    }

}
