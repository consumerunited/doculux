/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.goji.docusign.json.model;

/**
 *
 * @author paul.ye
 */
public enum DefaultUserSettings {

    canManageAccount("false"),
    canSendEnvelope("true"),
    locale("en"),
    canSendAPIRequests("false"),
    apiAccountWideAccess("true"),
    enableVaulting("false"),
    vaultingMode("none"),
    enableTransactionPoint("false"),
    enableSequentialSigningAPI("true"),
    enableSquentialSigningUI("true"),
    enableDSPro("false"),
    powerFormAdmin("false"),
    powerFormUser("true"),
    canEditSharedAddressbook("use_private_and_shared"),
    canManageTemplates("use"),
    enableSignOnPaperOverride("false"),
    enableSignerAttachments("true"),
    allowSendOnBehalfOf("false"),
    allowRecipientLanguageSelection("true"),
    canLockEnvelopes("true"),
    canUseScratchpad("true"),
    canCreateWorkspaces("false"),
    allowEmailChange("true"),
    allowPasswordChange("true"),
    bulksend("false"),
    selfSignedRecipientEmailDocument("include_pdf");

    private final String text;

    private DefaultUserSettings(final String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }
    
}
