/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.goji.docusign.json.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author paul.ye
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class FolderItem {
    @JsonProperty
    private String ownerName;
    @JsonProperty
    private String envelopeId;
    @JsonProperty
    private String envelopeUri;
    @JsonProperty
    private String status;
    @JsonProperty
    private String senderName;
    @JsonProperty
    private String senderEmail;
    @JsonProperty
    private String createdDateTime;
    @JsonProperty
    private String subject;
    @JsonProperty
    private String sentDateTime;

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getEnvelopeId() {
        return envelopeId;
    }

    public void setEnvelopeId(String evnelopeId) {
        this.envelopeId = evnelopeId;
    }

    public String getEnvelopeUri() {
        return envelopeUri;
    }

    public void setEnvelopeUri(String envelopeUri) {
        this.envelopeUri = envelopeUri;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getSenderEmail() {
        return senderEmail;
    }

    public void setSenderEmail(String senderEmail) {
        this.senderEmail = senderEmail;
    }

    public String getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(String createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }
    
    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("  {\n");
        sb.append("     ").append("ownerName : ").append(this.ownerName).append(", \n");
        sb.append("     ").append("envelopeId : ").append(this.envelopeId).append(", \n");
        sb.append("     ").append("envelopeUri : ").append(this.envelopeUri).append(", \n");
        sb.append("     ").append("status : ").append(this.status).append(", \n");
        sb.append("     ").append("senderName : ").append(this.senderName).append(", \n");
        sb.append("     ").append("senderEmail : ").append(this.senderEmail).append(", \n");
        sb.append("     ").append("createdDateTime : ").append(this.createdDateTime).append(", \n");
        sb.append("     ").append("subject : ").append(this.subject).append(", \n");
        if(this.sentDateTime!=null){
            sb.append("     ").append("sentDateTime : ").append(this.sentDateTime).append(", \n");
        }
        sb.append("  }\n");
        return sb.toString();
    }

    public String getSentDateTime() {
        return sentDateTime;
    }

    public void setSentDateTime(String sentDateTime) {
        this.sentDateTime = sentDateTime;
    }
}
