/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.goji.docusign.json.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author paul.ye
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Folder {

    /*
    public Folder(@JsonProperty("ownerUserName") String ownerUserName,
            @JsonProperty("ownerEmail") String ownerEmail,
            @JsonProperty("ownerUserId") String ownerUserId,
            @JsonProperty("type") String type,
            @JsonProperty("name") String name,
            @JsonProperty("uri") String uri,
            @JsonProperty("folderId") String folderId) {
        this.ownerUserName = ownerUserName;
        this.ownerEmail = ownerEmail;
        this.ownerUserId = ownerUserId;
        this.type = type;
        this.name = name;
        this.uri = uri;
        this.folderId = folderId;
    }*/

    @JsonProperty("ownerUserName")
    private String ownerUserName;
    @JsonProperty("ownerEmail")
    private String ownerEmail;
    @JsonProperty("ownerUserId")
    private String ownerUserId;
    @JsonProperty("type")
    private String type;
    @JsonProperty("name")
    private String name;
    @JsonProperty("uri")
    private String uri;
    @JsonProperty("folderId")
    private String folderId;
    @JsonProperty("filter")
    private FolderFilter filter;

    public String getOwnerUserName() {
        return ownerUserName;
    }


    public void setOwnerUserName(String ownerUserName) {
        this.ownerUserName = ownerUserName;
    }

    public String getOwnerEmail() {
        return ownerEmail;
    }


    public void setOwnerEmail(String ownerEmail) {
        this.ownerEmail = ownerEmail;
    }

    public String getOwnerUserId() {
        return ownerUserId;
    }


    public void setOwnerUserId(String ownerUserId) {
        this.ownerUserId = ownerUserId;
    }

    public String getType() {
        return type;
    }


    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }

    public String getUri() {
        return uri;
    }


    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getFolderId() {
        return folderId;
    }


    public void setFolderId(String folderId) {
        this.folderId = folderId;
    }
    
    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("  {\n");
        sb.append("    ").append("folderId: " + this.folderId).append(",\n");
        sb.append("    ").append("uri: " + this.uri).append(",\n");
        sb.append("    ").append("ownerEmail: "+this.ownerEmail).append(",\n");
        sb.append("    ").append("ownerUserId: " +this.ownerUserId).append(",\n");
        sb.append("    ").append("ownerUserName: "+this.ownerUserName).append(",\n");
        sb.append("    ").append("type: " +this.type).append(",\n");
        sb.append("    ").append("name: " +this.name).append("\n");
        sb.append("  }\n");
        return sb.toString();
    }

    public FolderFilter getFilter() {
        return filter;
    }

    public void setFilter(FolderFilter filter) {
        this.filter = filter;
    }

}
