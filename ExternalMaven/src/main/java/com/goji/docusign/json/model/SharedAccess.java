/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.goji.docusign.json.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

/**
 *
 * @author paul.ye
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SharedAccess {
    @JsonProperty
    private List<SharedAccessItem> sharedAccess;

    public List<SharedAccessItem> getSharedAccess() {
        return sharedAccess;
    }

    public void setSharedAccess(List<SharedAccessItem> sharedAccess) {
        this.sharedAccess = sharedAccess;
    }
}
