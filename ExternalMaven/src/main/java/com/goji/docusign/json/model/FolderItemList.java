/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.goji.docusign.json.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

/**
 *
 * @author paul.ye
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class FolderItemList {
    @JsonProperty
    private String resultSetSize;
    @JsonProperty
    private String startPosition;
    @JsonProperty
    private String endPosition;
    @JsonProperty
    private String totalSetSize;
    @JsonProperty
    private String previousUri;
    @JsonProperty
    private String nextUri;
    @JsonProperty
    private List<FolderItem> folderItems;

    public String getResultSetSize() {
        return resultSetSize;
    }

    public void setResultSetSize(String resultSetSize) {
        this.resultSetSize = resultSetSize;
    }

    public String getStartPosition() {
        return startPosition;
    }

    public void setStartPosition(String startPosition) {
        this.startPosition = startPosition;
    }

    public String getEndPosition() {
        return endPosition;
    }

    public void setEndPosition(String endPosition) {
        this.endPosition = endPosition;
    }

    public String getTotalSetSize() {
        return totalSetSize;
    }

    public void setTotalSetSize(String totalSetSize) {
        this.totalSetSize = totalSetSize;
    }

    public String getPreviousUri() {
        return previousUri;
    }

    public void setPreviousUri(String previousUri) {
        this.previousUri = previousUri;
    }

    public String getNextUri() {
        return nextUri;
    }

    public void setNextUri(String nextUri) {
        this.nextUri = nextUri;
    }

    public List<FolderItem> getFolderItems() {
        return folderItems;
    }

    public void setFolderItems(List<FolderItem> folderItems) {
        this.folderItems = folderItems;
    }
    
    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("{\n");
        sb.append("  resultSetSize : ").append(this.resultSetSize).append(", \n");
        sb.append("  startPosition : ").append(this.startPosition).append(", \n");
        sb.append("  endPosition : ").append(this.endPosition).append(", \n");
        sb.append("  totalSetSize : ").append(this.totalSetSize).append(", \n");
        sb.append("  previousUri : ").append(this.previousUri).append(", \n");
        sb.append("  nextUri : ").append(this.nextUri).append(", \n");
        sb.append("  folderItems : ").append("[ \n");
        if(folderItems!=null && folderItems.size()>0){
            for(FolderItem fi : folderItems){
                sb.append(fi);
            }
        }
        sb.append("  ] \n");
        sb.append("}\n");
        return sb.toString();
    }
}
