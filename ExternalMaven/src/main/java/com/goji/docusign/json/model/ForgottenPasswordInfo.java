/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.goji.docusign.json.model;

/**
 *
 * @author paul.ye
 */
public class ForgottenPasswordInfo {
    private String forgottenPasswordAnswer1;
    private String forgottenPasswordQuestion1;

    public String getForgottenPasswordAnswer1() {
        return forgottenPasswordAnswer1;
    }

    public void setForgottenPasswordAnswer1(String forgottenPasswordAnswer1) {
        this.forgottenPasswordAnswer1 = forgottenPasswordAnswer1;
    }

    public String getForgottenPasswordQuestion1() {
        return forgottenPasswordQuestion1;
    }

    public void setForgottenPasswordQuestion1(String forgottenPasswordQuestion1) {
        this.forgottenPasswordQuestion1 = forgottenPasswordQuestion1;
    }
}
