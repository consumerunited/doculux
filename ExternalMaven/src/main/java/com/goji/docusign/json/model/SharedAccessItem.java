/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.goji.docusign.json.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

/**
 *
 * @author paul.ye
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SharedAccessItem {
    @JsonProperty
    private User user;
    @JsonProperty
    private List<SharedEnvelope> envelopes;
    

    public List<SharedEnvelope> getEnvelopes() {
        return envelopes;
    }

    public void setEnvelopes(List<SharedEnvelope> envelopes) {
        this.envelopes = envelopes;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
