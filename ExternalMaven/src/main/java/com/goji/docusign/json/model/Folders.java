/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.goji.docusign.json.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

/**
 *
 * @author paul.ye
 */
@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Folders {
    @JsonProperty
    private List<Folder> folders;

    public List<Folder> getFolders() {
        return folders;
    }

    public void setFolders(List<Folder> folders) {
        this.folders = folders;
    }
    
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("{ folders: [\n");
        if(this.folders!=null&&this.folders.size()>0){
            for(Folder ff : folders){
                sb.append(ff);
            }
        }
        sb.append("]}\n");
        return sb.toString();
    }
}
