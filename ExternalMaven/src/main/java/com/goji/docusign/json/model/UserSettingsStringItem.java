/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.goji.docusign.json.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author paul.ye
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserSettingsStringItem  extends UserSettingsItem{
    
    @JsonProperty
    private String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
    
    public UserSettingsStringItem(String name, String value){
        this.name = name;
        this.value = value;
    }
    
    @Override
    public String toString(){
        return "{\n    name:" + name + ",\n    value:" + value+"\n}\n";
    }
}
