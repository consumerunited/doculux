/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.goji.docusign.resources;

import java.io.IOException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.http.conn.ssl.TrustStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author paul.ye
 */
@Service
@Produces("application/json")
@Path("/void")
public class VoidResource {

    private static Logger logger = Logger.getLogger(VoidResource.class.getName());

    static {
        try {
            FileHandler fh;
            fh = new FileHandler("voidBox.log");
            logger.addHandler(fh);
        } catch (IOException ex) {
            logger.log(Level.SEVERE, null, ex);
        } catch (SecurityException ex) {
            logger.log(Level.SEVERE, null, ex);
        }
    }

    @GET
    public Response check() {
        return Response.status(200).entity("Void").build();
    }

    @POST
    @Path("/voidbox")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response voidBox(String input) {
        System.out.println(input);
        logger.info(new Date() + " Input: \n" + input);
        return Response.status(Response.Status.OK).entity(input).build();
    }

}
